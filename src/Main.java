import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    public static int fieldSize = 1000;

    public static void main(String[] args) {
        int step = 1;
        Board b = new Board(4);
        //b.putRandomQueens(fieldSize);
        b.set(0,1,true);
        b.set(1,1,true);
        b.set(2,3, true);
        b.set(3,1, true);
        //b.printOccupied();
        b.print();
        List<Integer> a = b.getConflicts(0);
        System.out.println(a);


        // b.print();

        System.out.println("Going for " + fieldSize + " queens");
        Board nb = new Board(fieldSize);
        nb.putRandomQueens(fieldSize);
        System.out.println("INIT")
        ;nb.print();
        //Get least conflicted queen
        nb.solve();

        System.out.println("SOLVED "+fieldSize+" queens!");
        nb.print();


        //Get least conflicted pos


       /* while(!nb.isSolved()) {
            step++;
            nb.moveRandomQueen();

        }
        nb.print();
        System.out.println("Solved after "+step+" steps");*/

    }



}
