
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {

    private int size;
    private boolean[][] board;
    public int[] occupied;

    public void solve() {
        long starttime = System.currentTimeMillis();
        int currQueen;
        int stepcount = 0;

        while(true) {
            if (isSolved()) {
                long end = System.currentTimeMillis();
                long runtime = end - starttime;
                System.out.println("Solved after "+stepcount+" steps" + "after " + runtime + " ms");
                return;
            } else {
                //print();
                if (System.nanoTime() % 2 == 0) System.out.println("Stepcount: "+stepcount);
                currQueen = getLeastConflictedQueen();
                moveToLeastConflicted(currQueen);
                stepcount++;
            }
        }
    }

    public Board(int size) {
        this.size = size;
        init(size);

    }

    public void init(int size) {
        this.board = new boolean[size][size];
        this.occupied = new int[size];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size-1; y++) {
                board[x][y] = false;
               // System.out.println("INIT: BOARD["+x+"]"+"["+y+"]"+"="+board[x][y]);
            }
        }
    }

    public void set(int x, int y, boolean val) {
        board[x][y] = val;
        occupied[x] = y;
    }
    public int get(int x) {
        return occupied[x];
    }

    public void print() {
       if(size < 50) {
           System.out.println();

           for (int x = size - 1; x >= 0; x--) {
               System.out.print(x + "\t");
               for (int y = 0; y < size; y++) System.out.print((board[y][x] ? "CROWN" : "[\t]") + "\t");
               System.out.println();
           }
           for (int x = 0; x < size; x++) System.out.print("\t" + x + "\t");
           System.out.println();
       }
    }

    public void printOccupied() {
        System.out.println("Occupied fields:");
        for(int x = 0; x < size ;x++) {
            int y = occupied[x];
            System.out.println("("+x + "," + y+")");
        }
    }

    public boolean isSolved() {
    //    int comps = 0;
        for (int i = size-1; i >= 0; i--) {
            for (int x = 0; x < i ; x++) {
             //   for (int x = size-1; x >= 0; x--) {
        /*        if (x == i) {
                    x--;
                } else {*/
                int offset = i - x;
                int negoffset = offset * -1;
                int posDiff = occupied[i] - occupied[x];

                if (posDiff == offset || posDiff == negoffset) {
                    return false;
                } else {
                    if (!(x - 1 < 0)) {
                        if (occupied[i] == occupied[x]) {
                            return false;
                        }
                    }
                }
               // }
                //comps++;
            }
        }
     //   System.out.println("Compared: " +comps);
        return true;
    }

    public void moveRandomQueen() {
        int randQueen = new Random().nextInt(size);
        int pos = occupied[randQueen];
        int randPos = new Random().nextInt(size);
        set(randQueen, pos, false);
        boolean moved = false;
        while (!moved) {
            randPos = new Random().nextInt(size);
            if (pos != randPos) {

                occupied[randQueen] = randPos;
                set(randQueen,randPos, true);
                moved = true;
            }

        }
    }

    public void putRandomQueens(int num) {
        if(num > size*size) {
            System.out.println("Queen overload.");
        } else {
            for (int i = 0; i < num; i++) {
                int randY = new Random().nextInt(size);
                set(i, randY, true);
            }
        }
    }

    // Find a queen, that has minimal conflicts
    public int getLeastConflictedQueen() {
        int leastConflicted = -1;
        int leastConflicts = Integer.MAX_VALUE;
        List<Integer> equalConf = new ArrayList<>();
        for(int i = 0;i <size; i++) {
           // for (int i = size; i > 0; i--) {
         //   print();
            int conflicts = getConflicts(i).size();
          //  System.out.println("Conflicts of queen "+ i+":"+conflicts);
            if (conflicts < leastConflicts  && conflicts > 0) {
                leastConflicts = conflicts;
                equalConf.clear();
             //   System.out.println("\t Cleared equalconf.");
                equalConf.add(i);
              //  System.out.println("\t Actually Least conflicted Queen: "+ i+" conf: "+conflicts);
            } else
                if (conflicts == leastConflicts) {
                    if(!equalConf.contains(leastConflicted)) equalConf.add(i);
                }
        }
        leastConflicted = equalConf.get(new Random().nextInt(equalConf.size()));
     //   System.out.println("Least conflicted columns: "+equalConf);
     //   System.out.println("rand(LC) --> "+leastConflicted);

    return leastConflicted;
    }

    // Pick a queen thag has minimal conflicts, then choose a random of the conflicting queens, move it so that it has
    // equal or less conflicts than before
    public void moveToLeastConflicted(int queen) {

        int start = get(queen);
        int leastConflicted = new Random().nextInt(size);
        int leastConflicts = Integer.MAX_VALUE;
        List<Integer> equalConf = new ArrayList<>();
        List<Integer> attackable = getConflicts(queen);
        //System.out.println("Attackables of least conflicted queen: " + attackable);
        int chosenOne = attackable.get(new Random().nextInt(attackable.size()));
      //  System.out.println("Chosen one -> "+chosenOne);
        for (int i = 0; i < size; i++) {
       // System.out.println("Queen: "+queen+"\t Row: "+i);
            moveQueen(chosenOne, i);
            int conflicts = getConflicts(chosenOne).size();
        //    System.out.println("\tConflicts of Row "+ i+":"+conflicts);
            if (conflicts < leastConflicts) {
        //        System.out.println("Num of conflicts of queen "+ queen+"="+conflicts+" -- Least conflicted row: "+i);
                leastConflicts = conflicts;
                equalConf.clear();
              //  System.out.println("\t Cleared equalconf: New least ");
                equalConf.add(i);
                //System.out.println("\t Actually Least conflicted Row: "+ i+" conf: "+conflicts);
            } else if (conflicts == leastConflicts) {
                equalConf.add(i);
            }
        }
        leastConflicted = equalConf.get(new Random().nextInt(equalConf.size()));
      //  System.out.println("Least conflicted rows: "+equalConf);
       // System.out.println("rand(LCS) -->" + leastConflicted);
       // System.out.println("Queen movement: ("+chosenOne+","+start+") --> ("+chosenOne+","+leastConflicted+")");
        moveQueen(chosenOne, leastConflicted);
    }
    public void moveQueen(int queen, int pos) {
        set(queen, get(queen), false);
        set(queen, pos, true);
    }
      public List<Integer> getConflicts(int queen) {
        List<Integer> conflicts = new ArrayList<>();

        int pos = get(queen);
        for (int i = 0; i < size; i++) {
            if (!(i == queen && occupied[i] == pos)) {

                //Horizontal Sight
                if (board[i][pos]) {
                    conflicts.add(i);
                 //   System.out.println("Conflict horizontal - \tPos: "+ i +", "+ pos);

                }
            }
        }
        //Falling diagonal
        int x = queen;
        int y = pos;
        boolean breakup = false;
            //p1
        while(!breakup /*|| (conflicts.size() == 5)*/) {
            x++; y--;
           // if (conflicts.size() == 5) breakup = true; else
            if (x >= size || y < 0) breakup = true;
            else {
                if(board[x][y]) {
                    conflicts.add(x);
                  //  System.out.println("Conflict, Falling diagonal, P 1 - \tPos: "+ x +", "+y);

                }
            }
        }
        x = queen;
        y = pos;
        breakup = false;
        //p2
        while(!breakup /*|| (conflicts.size() == 5)*/) {
            x--; y++;
            // if (conflicts.size() == 5) breakup = true; else
            if (x < 0 || y >= size) breakup = true;
            else {
                if(board[x][y]) {
                  //  System.out.println("Conflict, Falling diagonal, P2 - \tPos: "+ x +", "+y);
                    conflicts.add(x);
                }
            }
        } breakup = false;
        //rising diagonal
        //p1
          x = queen;
          y = pos;
        while(!breakup) {
            x++; y++;
            // if (conflicts.size() == 5) breakup = true; else
            if (x >= size || y >= size) breakup = true;
            else {
                if(board[x][y]) {
                  //  System.out.println("Conflict, Rising diagonal, P1 - \tPos: "+ x +", "+y);
                    conflicts.add(x);
                }
            }
        } breakup = false;
        //p2
          x = queen;
          y = pos;
        while(!breakup /*|| (conflicts.size() == 5)*/) {
            x--; y--;
            // if (conflicts.size() == 5) breakup = true; else
            if (x < 0 || y < 0) breakup = true;
            else {
                if(board[x][y]) {
                  //  System.out.println("Conflict, Rising diagonal, P2 - \tPos: "+ x +", "+y);
                    conflicts.add(x);
                }
            }
        }
        return conflicts;
    }
}
