import java.util.Iterator;

public class Coordinates {
    int x;
    int y;


    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return ("("+x+","+y+")");
    }
}

  /* public static Board see(int[] occupied, Board b) {
        Board radiance = b;
        int queenX = coordinates.getX();
        int queenY = coordinates.getY();

        radiance.set(queenX, queenY, true);

        //Vertical Sight
        for (int i = 0; i < fieldSize; i++) {
            radiance.set(queenX, i, true);
        }
        //Horizontal Sight
        for (int i = 0; i < fieldSize; i++) {
            radiance.set(i, queenY, true);
        }
        //Falling diagonal
        int x = queenX;
        int y = queenY;
        boolean breakup = false;
            //p1
        while(!breakup) {
            x++; y--;
            if (x >= fieldSize || y < 0) breakup = true;
            else {
                radiance.set(x,y,true);
            }
        }
        x = queenX;
        y = queenY;
        breakup = false;
        //p2
        while(!breakup) {
            x--; y++;
            if (x < 0 || y >= fieldSize) breakup = true;
            else {
                radiance.set(x,y,true);
            }
        } breakup = false;
        //rising diagonal
        //p1
        x = queenX;
        y = queenY;
        while(!breakup) {
            x++; y++;
            if (x >= fieldSize || y >= fieldSize) breakup = true;
            else {
                radiance.set(x,y,true);
            }
        } breakup = false;
        //p2
        x = queenX;
        y = queenY;
        while(!breakup) {
            x--; y--;
            if (x < 0 || y < 0) breakup = true;
            else {
                radiance.set(x,y,true);
            }
        } breakup = false;
        radiance.print();
        return radiance;
    }*/